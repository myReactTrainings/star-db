import React from 'react';

import './spinner.scss'

const Spinner = () => {
    return (
        <div className="spinner-wrapper">
            <div className="loadingio-spinner-double-ring-55s5t2ja7ia">
                <div className="ldio-snzaziguegb">
                    <div></div>
                    <div></div>
                    <div><div></div></div>
                    <div><div></div></div>
                </div>
            </div>
        </div>
    )
};

export default Spinner;